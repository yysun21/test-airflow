from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators.bash_operator import BashOperator

from airflow.operators.python_operator import PythonOperator

default_args = {
    'owner': 'airflow',
    'start_date': datetime(2018, 1, 4)
}

dag = DAG('test_scheduled', default_args=default_args, schedule_interval=timedelta(1/24))


def hello_airflow():
    print("Hello AirFlow!")


sleep1 = BashOperator(
    task_id='sleep1',
    bash_command='sleep 1',
    retries=1,
    dag=dag
)


helloAirFlow = PythonOperator(
    task_id='hello_airflow',
    python_callable=hello_airflow,
    dag=dag
)

helloAirFlow.set_upstream(sleep1)
